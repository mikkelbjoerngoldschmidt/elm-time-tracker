## Elm todoist time tracker
This project aims to make it easy to track time spent in different kinds
of projects.
Data about available tasks are fetched from the users Todoist account.
The Todoist account must be premium, as the project interface needs you to 
provide a filter to fetch appropriate tasks, which is a Todoist pro feature.