module Components exposing
    ( Component
    , button
    , clickableText
    , container
    , defaultMessage
    , map
    , newLine
    , page
    , panel
    , text
    , textInputField
    , warningMessage
    )

import Bulma.CDN exposing (stylesheet)
import Bulma.Components
import Bulma.Elements exposing (ButtonModifiers, easyButton)
import Bulma.Form
import Bulma.Modifiers exposing (Color)
import Html exposing (Html)
import Html.Attributes exposing (href, rel)
import Html.Events
import Style


type Component msg
    = Component (Html msg)


{-| Internal module function to extract the html of a component.
-}
viewComponent : Component msg -> Html msg
viewComponent component =
    case component of
        Component content ->
            content


fontAwesomeCDN : Html msg
fontAwesomeCDN =
    Html.node "link"
        [ rel "stylesheet"
        , href "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
        ]
        []


map : (a -> b) -> Component a -> Component b
map f (Component msg) =
    Component (Html.map f msg)


{-| Views a page given the components within it
-}
page : List (Component msg) -> Html msg
page components =
    Html.main_ [] <|
        List.append [ stylesheet, fontAwesomeCDN ] (List.map viewComponent components)


message : Color -> String -> Component msg -> Component msg
message messageColor header body =
    Component <|
        Bulma.Components.message
            { color = messageColor
            , size = Bulma.Modifiers.Medium
            }
            []
            [ Bulma.Components.messageHeader []
                [ Html.text header ]
            , Bulma.Components.messageBody []
                [ viewComponent body ]
            ]


defaultMessage : String -> Component msg -> Component msg
defaultMessage =
    message Bulma.Modifiers.Default


warningMessage : String -> Component msg -> Component msg
warningMessage =
    message Bulma.Modifiers.Warning


textInputField : (String -> msg) -> Component msg
textInputField msg =
    Component <|
        Bulma.Form.field []
            [ Bulma.Form.controlInput
                Style.defaultControlInputModifiers
                []
                [ Html.Events.onInput msg ]
                []
            ]


{-| Takes a list of components and wraps them in a single Component
-}
container : List (Component msg) -> Component msg
container components =
    Component (Html.div [] <| List.map viewComponent components)


{-| Forces a break of the current line
-}
newLine : Component msg
newLine =
    Component <| Html.br [] []


button : String -> msg -> Component msg
button buttonText msg =
    Component <| easyButton Style.defaultButtonModifiers [] msg buttonText


text : String -> Component msg
text value =
    Component <| Html.text value


panel : String -> List ( Component msg, Bool ) -> List (Component msg) -> (String -> msg) -> Component msg
panel title tabs elements searchMsg =
    Component <|
        Bulma.Components.panel [] <|
            [ Bulma.Components.panelHeading [] [ Html.text title ]
            , Bulma.Components.panelBlock False
                []
                [ viewComponent <| textInputField searchMsg
                ]
            , Bulma.Components.panelTabs [] <|
                (tabs
                    |> List.map (Tuple.mapFirst viewComponent)
                    |> List.map (Tuple.mapFirst List.singleton)
                    |> List.map (\( html, chosen ) -> Bulma.Components.panelTab chosen [] html)
                )
            ]
                ++ [ elements
                        |> List.map viewComponent
                        |> Bulma.Components.panelLink False []
                   ]


{-| Makes a text component that can be clicked and send the provided msg
-}
clickableText : msg -> String -> Component msg
clickableText msg value =
    Component <| Html.div [ Html.Events.onClick msg ] [ Html.text value ]
