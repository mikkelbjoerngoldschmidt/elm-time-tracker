module Style exposing (defaultButtonModifiers, defaultControlInputModifiers)

import Bulma.Elements exposing (ButtonModifiers, buttonModifiers)
import Bulma.Form exposing (ControlInputModifiers, controlInputModifiers)


defaultButtonModifiers : ButtonModifiers msg
defaultButtonModifiers =
    buttonModifiers


defaultControlInputModifiers : ControlInputModifiers msg
defaultControlInputModifiers =
    controlInputModifiers
