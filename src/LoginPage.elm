module LoginPage exposing (Model, Msg(..), initialModel, view, update)

import Components exposing (Component, text)


type alias Model =
    String


initialModel =
    ""


type Msg
    = NewToken String
    | Submit


view : Model -> Component Msg
view model =
    Components.warningMessage
        "Missing token"
    <|
        Components.container
            [ text "Please provide a token to get your data from Todoist."
            , Components.textInputField NewToken
            , Components.button "Submit token" Submit
            ]

{-| Updates the model of the input page.
Returns the new model of the page and a bool indicated if a login request has been submitted.
-}
update : Msg -> Model -> (Model, Bool)
update msg model =
    case msg of
        NewToken token ->
            (token, False)

        Submit ->
            (model, True)
