module Main exposing (main)

import Browser
import Components exposing (text)
import LoginPage
import Time
import TimeTracker exposing (Msg(..))


type Msg
    = TimeTrackerMessage TimeTracker.Msg
    | LoginPageMsg LoginPage.Msg


type Model
    = LoginPageModel LoginPage.Model
    | TimeTrackerModel TimeTracker.Model


main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


init : () -> ( Model, Cmd msg )
init _ =
    ( LoginPageModel LoginPage.initialModel, Cmd.none )


update : Msg -> Model -> ( Model, Cmd msg )
update msg superModel =
    case msg of
        LoginPageMsg loginMsg ->
            case superModel of
                LoginPageModel model ->
                    let
                        ( token, submitted ) =
                            LoginPage.update loginMsg model
                    in
                    if submitted then
                        ( TimeTrackerModel <| TimeTracker.defaultModelWithToken token, Cmd.none )

                    else
                        ( LoginPageModel token, Cmd.none )

                _ ->
                    errorPage

        TimeTrackerMessage mainMsg ->
            case superModel of
                TimeTrackerModel model ->
                    Tuple.mapFirst TimeTrackerModel <| TimeTracker.update mainMsg model

                _ ->
                    errorPage


view modelState =
    Components.page <|
        List.singleton <|
            case modelState of
                LoginPageModel model ->
                    Components.map LoginPageMsg <| LoginPage.view model

                TimeTrackerModel model ->
                    Components.map TimeTrackerMessage <| TimeTracker.view model


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        TimeTrackerModel _ ->
            Sub.map TimeTrackerMessage <| Time.every 100 TimeUpdated

        _ ->
            Sub.none


{-| A state of the model indicating that a Msg has been sent, that did not
-}
errorPage : ( Model, Cmd msg )
errorPage =
    init ()
