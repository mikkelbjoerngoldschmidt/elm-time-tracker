module TimeTracker exposing (Model, Msg(..), Project(..), defaultModelWithToken, update, view)

import Components exposing (Component, page, text)
import Time exposing (Posix)
import TodoistRest as Todoist


type Project
    = Project { name : String, filter : String }


type TimerState
    = NotRunning
    | Running { startTime : Posix, task : Todoist.Task }


type TimeTracker
    = TimeTracker
        { project : Project
        , tasks : Maybe (List Todoist.Task)
        , state : TimerState
        }


type alias Model =
    { currentTime : Posix
    , todoistToken : Todoist.Token
    , timeTracker : Maybe TimeTracker
    , projects : List Project
    , extraFilter : String
    }


type Msg
    = TimeUpdated Posix
    | NewSearchString String
    | ChooseNewProject Project


view model =
    let
        timeTracker =
            model.timeTracker

        maybeProject =
            case timeTracker of
                Nothing ->
                    Nothing

                Just (TimeTracker tracker) ->
                    Just tracker.project
    in
    Components.container
        [ viewProjectList model.projects
            (Maybe.withDefault [] <|
                getTasksFromModel model
            )
            maybeProject
            NewSearchString
        ]


{-| Returns the name of the provided project.
-}
getProjectName : Project -> String
getProjectName (Project project) =
    project.name


exampleProjects : List Project
exampleProjects =
    List.map Project
        [ { name = "DTU", filter = "##DTU" }
        , { name = "Concat", filter = "##Concat" }
        , { name = "Firefy", filter = "##Firefly" }
        , { name = "Zdani", filter = "##Zdani" }
        , { name = "Fitness", filter = "##Fitness" }
        ]


defaultModelWithToken : Todoist.Token -> Model
defaultModelWithToken token =
    { currentTime = Time.millisToPosix 0
    , projects = exampleProjects
    , timeTracker = Nothing
    , todoistToken = token
    , extraFilter = ""
    }


updateTimeTrackerProject : Project -> TimeTracker -> TimeTracker
updateTimeTrackerProject project (TimeTracker timeTracker) =
    TimeTracker { timeTracker | project = project }


newTimeTracker : Project -> TimeTracker
newTimeTracker project =
    TimeTracker { project = project, tasks = Nothing, state = NotRunning }


update : Msg -> Model -> ( Model, Cmd msg )
update msg model =
    case msg of
        TimeUpdated now ->
            ( { model | currentTime = now }, Cmd.none )

        NewSearchString filter ->
            ( { model | extraFilter = filter }, Cmd.none )

        ChooseNewProject project ->
            ( { model | timeTracker = Just <| newTimeTracker project }, Cmd.none )


{-| Returns a list of tasks in from the TimeTracker in the model.
In case no time tracker is present or no tasks are contained in it - Nothing is returned.
-}
getTasksFromModel : Model -> Maybe (List Todoist.Task)
getTasksFromModel model =
    case model.timeTracker of
        Nothing ->
            Nothing

        Just (TimeTracker records) ->
            records.tasks


{-| Makes clickable text sending a Msg to change the current project to the provided one.
-}
viewProjectButton : Project -> Component Msg
viewProjectButton (Project project) =
    Components.clickableText (ChooseNewProject (Project project)) project.name


viewProjectList : List Project -> List Todoist.Task -> Maybe Project -> (String -> Msg) -> Component Msg
viewProjectList projects tasks maybeChosenProject =
    let
        isChosenProject =
            \(Project project) -> case maybeChosenProject of
                Nothing ->
                    False
                Just (Project chosenProject) ->
                    project.name == chosenProject.name
    in
    Components.panel
        "Projects"
        (List.map (\project -> ( viewProjectButton project, isChosenProject project )) projects)
        (tasks |> List.map .content |> List.map text)
